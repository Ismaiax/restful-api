const Joi = require('joi');
const express = require('express');
const jwt = require('jsonwebtoken');



const registerValidation = data => {
	const RegistrationSchema = Joi.object({
		name: Joi.string().min(3).required(),
		email: Joi.string().min(6).required().email(),
		password: Joi.string().min(4).required()
	});

	return RegistrationSchema.validate(data);
};

const loginValidation = data => {
	const LoginSchema = Joi.object({
		email: Joi.string().min(6).required().email(),
		password: Joi.string().min(4).required()
	});

	return LoginSchema.validate(data);
};

const protectedUri = express.Router(); 
protectedUri.use((req, res, next) => {
    const token = req.headers['access-token'];
	
    if (token) {
      jwt.verify(token, process.env.TOKEN_SECRET, (err, decoded) => {      
        if (err) {
          return res.status(401).json({ mensaje: 'Token inválido' });    
        } else {
          req.decoded = decoded;    
          next();
        }
      });
    } else {
      res.status(401).json({ 
          mensaje: 'Token no indicado.' 
      });
    }
 });

module.exports.loginValidation = loginValidation;
module.exports.registerValidation = registerValidation;
module.exports.protectedUri = protectedUri;