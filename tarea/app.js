const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const bodyParser = require('body-parser');
require('dotenv').config();

const app = express();

app.use(cors())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

// Importando rutas
const postsRoute = require('./routes/posts');
const userRoute = require('./routes/user');


app.set('port', process.env.PORT || 3000);
app.use('/note', postsRoute);
app.use('/user', userRoute);


// Página de inicio
app.get('/', (req, res) => {
	res.send('API REST NodeJS MongoDB')
});

// Connect MongoDB
mongoose.connect(process.env.MONGODB_DATABASE, { 
    useUnifiedTopology: true, 
    useNewUrlParser: true  
}, () => {
	console.log("Conectado a la base de datos")
});

app.listen(app.get('port'), () => {
    console.log(`Servidor corriendo en el puerto ${app.get('port')}`);
});