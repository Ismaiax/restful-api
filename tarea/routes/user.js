const express = require('express');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const router = express.Router();
require('dotenv').config();

// Importando los modelos
const User = require('../models/User');
const Post = require('../models/Post');

// Importando validación
const { registerValidation, loginValidation, protectedUri } = require('../validation');


/**
 * Función para registrar un usuario
 *
 * @author	Ismael Tello
 * @param	{string} 	email Estructura de correo electrónico
 * @param	{string} 	name Nombre del usuario mínimo 3 caracteres
 * @param	{string} 	password Password mínimo 4 caracteres
 * @return	{json} 		Código de estado de respuesta y detalles
 */
router.post('/register', async (req, res) => {

	// Validación de los datos al crear el usuario
	const { error } = registerValidation(req.body);
	if (error) return res.status(400).json({
		"message" : error.details[0].message,
		"details" : error
	});

	// Confirmar si el correo ya existe en la base de datos
	const emailExist = await User.findOne({ email: req.body.email });
	if(emailExist) return res.status(409).json({
		"message":"El correo ya existe"
	});

	// Encriptación de la contraseña
	const salt = await bcrypt.genSalt(10);
	const hashedPassword = await bcrypt.hash(req.body.password, salt);

	// Crear y guardar el usuario
	const user = new User({ 
		name: req.body.name,
		email: req.body.email,
		password: hashedPassword
	});

	try{
		const savedUser = await user.save();
		res.status(201);
		res.json({ 
			message : "Usuario creado con éxito",
			name: user.name, 
			email: user.email 
		});
	} catch (err) {
		res.status(400);
		res.json({ 
			"message": "Error al crear el usuario",
			"details": err 
		});
	}
});



/**
 * Función iniciar sesión
 *
 * @author	Ismael Tello
 * @param	{string} 	email Estructura de correo electrónico
 * @param	{string} 	password Password mínimo 4 caracteres
 * @return	{json} 		Código de estado de respuesta, token y json de detalles
 */
router.post('/login', async(req, res) => {

	// Error de validación
	const { error } = loginValidation(req.body);
	if (error) return res.status(400).json({
		message : "Error de validación",
		details : error.details[0].message
	});

	// Confirmar que el usuario (correo) existe
	const user = await User.findOne({ email: req.body.email });
	if(!user) return res.status(404).json({
		message: 'El usuario no está registrado'
	});

	// Confirmar el login del usurio
	const validPass = await bcrypt.compare(req.body.password, user.password)
	if(!validPass) return res.status(400).json({
		message : 'Contraseña incorrecta'
	});

	// Crear usuario y asignar contraseña con un token de expiración de media hora
	const token = jwt.sign({ _id: user._id, check:true }, process.env.TOKEN_SECRET, {
		expiresIn: 1800
	});
	res.status(200);
	res.header('access-token', token);
	res.json({
		message : "Inicio de sesión correcto",
		token : token,
		details: user
	});
});




/**
 * Función para borrar un usuario y dejar las notas
 *
 * @author	Ismael Tello
 * @param	{string} 	uid Id del usuario
 * @return	{json} 		Código de estado de respuesta y detalles
 */
router.delete('/delete/only', protectedUri, async(req, res) => {
	await User.findByIdAndDelete( req.body.uid )
		.then( respuesta => {

			if(!respuesta) {

				return res.status(400).json({
					message : 'Error al borrar el usuario',
					details : `El usuario no fue encontrado`
				})
			} else {
				return res.status(200).json({
					message : 'Usuario borrado con éxito',
					details : `El usuario eliminado fue: ${respuesta.name}`
				})
			}
		})
		.catch(err => {
			if (err) {
				console.log("error => " + err)
				res.status(409)
				res.json({
					message : 'Error al borrar el usuario',
					details : err
				})
			}
		});
	
});



/**
 * Función para borrar un usuario y sus notas relacionadas
 *
 * @author	Ismael Tello
 * @param	{string} 	uid Id del usuario
 * @return	{json} 		Código de estado de respuesta y detalles
 */
router.delete('/delete/all', protectedUri, async(req, res) => {
	try {
		const removedUser = await User.findByIdAndDelete({ _id: req.body.uid  });
		const removedPosts = await Post.deleteMany({ userID: req.body.uid });
		if(!removedUser || !removedPosts) {
			return res.status(404).json({
				message : "Error al borrar el usuario y/o sus notas"
			})
		} else {
			return res.status(200).json({
				message : "Borrado exitoso",
				details : {
					userDelete : removedUser,
					noteDelete : `Notas del usuario ${removedUser.name} borradas`,
					noteDetails: removedPosts 
				}

			});

		}
	} catch(err) {
		res.status(400);
		res.json({ message: err });
	}
});

module.exports = router;