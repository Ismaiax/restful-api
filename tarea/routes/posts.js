const express = require('express');
const router = express.Router();

// Importar modelos
const User = require('../models/User');
const Post = require('../models/Post');

// Importar validaciones
const { registerValidation, loginValidation, protectedUri } = require('../validation');
const { response } = require('express');


/**
 * Crear una nota para un usuario específico
 *
 * @author	Ismael Tello
 * @param	{string} 	uid Id del usuario
 * @param	{string} 	title Título de la nota
 * @param	{string} 	description Contenido de la nota
 * @return	{json} 		Código de estado de respuesta y detalles
 */
router.post('/user', protectedUri, async (req, res) => {

	// Validando estructura del ID
	var checkForHexRegExp = new RegExp("^[0-9a-fA-F]{24}$");
	if(!checkForHexRegExp.test(req.body.uid)) {
		return res.status(400).json({
			message : "ID de usuario en formato incorrecto"
		})
	}
	
	// Confirmar que el usuario existe
	const user = await User.find({"_id" :req.body.uid});
	if( user.length === 0) {

		res.status(404);
		res.json({
			message: 'El usuario no está registrado y la nota no fue creada'
		});

	} else {

		const post = new Post({
			userID: req.body.uid,
			title: req.body.title,
			description: req.body.description
		})

		try{
			const savedPost = await post.save();
			res.status(201);
			res.json({
				message : "Nota creada con éxito",
				details : savedPost
			})
		} catch (err) {
			res.status(400);
			res.json({ 
				message: "Error al crear la nota",
				details: err 
			})
		}

	}

	
});


/**
 * Obtener las notas de un usuario
 *
 * @author	Ismael Tello
 * @param	{string} 	uid Id del usuario
 * @return	{json} 		Código de estado de respuesta y detalles
 */
router.get('/user', async(req, res) => {
	if(!req.body.uid) return res.status(400).json({
		message: 'Error, falta indicar el ID del usuario'
	});

	try {
		const posts = await Post.find({ userID: { $eq: req.body.uid } });
		return res.status(200).json({
			message: `Se recuperaron ${posts.length} notas`,
			details : posts
		});
	} catch(err) {
		res.status(400).json({ message: err });
	}
});



/**
 * Obtener una nota en particular
 *
 * @author	Ismael Tello
 * @param	{string} 	uid Id del usuario
 * @param	{string} 	postId Id de la nota
 * @return	{json} 		Código de estado de respuesta y detalles
 */
router.get('/one', async(req, res) => {
	try {
		const post = await Post.findById(req.body.postId);
		if (post.userID === req.body.uid) {
			return res.status(200).json({
				message : 'Nota obtenida con éxtio',
				details : post
			});
		} else {
			return res.status(404).json({ message: "Usuario incorrecto" });
		}
	} catch(err) {
		res.status(400).json({ 
			message: 'Error al recuperar la nota',
			details: err 
		});
	}
});



/**
 * Actualizar el contenido de una nota
 *
 * @author	Ismael Tello
 * @param	{string} 	uid Id del usuario
 * @param	{string} 	postId Id de la nota
 * @param	{string} 	description Nuevo contenido de la nota
 * @return	{json} 		Código de estado de respuesta y detalles
 */
router.patch('/one', protectedUri,  async (req, res) => {
	try {
		const updatedPost = await Post.updateOne({ 
			_id: req.body.postId, 
			userID: { $eq: req.body.uid } 
		}, {
			$set: {
				description: req.body.description
			}
		});
		if(updatedPost.nModified === 0) {
			return res.status(400).json({
				message : "Error al actualizar la nota",
				details : updatedPost
			});
		} else {
			return res.status(200).json({
				message : "Nota actualizada con éxito"			
			});
		}
		
	} catch(err) {
		res.status(400).json({ message: err });
	}
});



/**
 * Borrar una nota
 *
 * @author	Ismael Tello
 * @param	{string} 	uid Id del usuario
 * @param	{string} 	postId Id de la nota
 * @return	{json} 		Código de estado de respuesta y detalles
 */
router.delete('/one', protectedUri, async(req, res) => {
	try {
		const removedPost = await Post.remove({ _id: req.body.postId, userID: { $eq: req.body.uid } });
		if(removedPost.deletedCount === 0) {
			return res.status(400).json({
				message : "Error al borrar la nota",
				details: removedPost
			});
		} else {
			return res.status(200).json({
				message : "Nota borrada con éxito",
				details: removedPost
			});
		}
	} catch(err) {
		res.status(400).json({ message: err });
	}
});

module.exports = router;