# Implementación de una solución basada en una aplicación web II #

Responsable del proyecto [Ismael Tello](isaiasismael.telloLI8@comunidadunir.net)

API RESTful para la creación de usuarios y notas.

# Estructura del proyecto
> Estructura de carpetas y nombrado de archivos

## Raíz

    .
    ├── tarea           # Aplicación
    ├── README.md
    ├── .env            # Archivo con variables globales
    └── package.json

La aplicación está realizada con NodeJS y una base de datos MongoDB. La carpeta básica contiene el proyecto,el archivo `package.json` y el archivo `.env` para la configuración de la creación del token y ubicación del servidor de base de datos (poco seguro en el repositorio pero se mantiene para fines educativos)

Una vez clonado el proyecto se ejecuta el comando para instalar los módulos utilizados:

   `npm install`

## Proyecto
    .
    ├── ...
    ├── models                  # Carpeta con los esquemas de la base de datos
    ├── routes                  # Carpeta con las rutas
    ├── app.js                  # Aplicación principal
    ├── validation.js           # Archivo con la configuración para validación de datos y el token
    └── ...

# Changelog
Comienzo del proyecto 15 de enero 2021
## [1.0.0] - 2021-01-15
### Added
- Se genera el proyecto principal con toda la estructura descrita en el presente documento